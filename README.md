# my helpful bash functions
```
# Thank you Mike
gpush ()
{
    branch=`git branch --no-color |grep '*' | cut -d ' ' -f 2`
    git push $@ origin ${branch}
}
# type less
gstat ()
{
    git status
}
# Some times a clean reset is needed
gpull ()
{
#    git clean -xfd
#    git reset --hard
    git pull
}

```
